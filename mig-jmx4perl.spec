%define cpan_name jmx4perl

%define jmx4perl_url %((grep /%{cpan_name} %{_sourcedir}/sources || \
                        grep /%{cpan_name}  %{_builddir}/sources || \
                        echo "x unknown") 2>/dev/null | perl -ane 'print("$F[1]")')
%define jmx4perl_tar %(basename %{jmx4perl_url})
%define jmx4perl_version %(echo %{jmx4perl_tar} | perl -pe 's/^%{cpan_name}-//; s/\\.tar\\.gz$//')
%define is_github %(echo %{jmx4perl_version} | perl -ne 'print /\\./ ? 0 : 1')
%if %{is_github}
%define rpm_version 1.12
%define rpm_release %(echo %{jmx4perl_version} | perl -pe 's/^(.......).+$/2.$1/')
%else
%define rpm_version %{jmx4perl_version}
%define rpm_release 1
%endif

Name:		mig-jmx4perl
Version:	%{rpm_version}
Release:	%{rpm_release}%{?dist}
Summary:	JMX access for Perl
License:	GPL-1.0+
Group:		Development/Libraries
Url:		http://search.cpan.org/dist/%{cpan_name}/
Source0:	%{jmx4perl_tar}
Source1:	sources
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:  perl
BuildRequires:  perl(Carp)
BuildRequires:  perl(Data::Dumper)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Getopt::Long)
BuildRequires:  perl(JSON) >= 2.12
BuildRequires:  perl(LWP::UserAgent)
BuildRequires:  perl(Module::Find)
BuildRequires:  perl(Scalar::Util)
BuildRequires:  perl(Sys::SigAction)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(URI) >= 1.35
BuildRequires:  perl(base)

%description
Jmx4Perl provides an alternate way for accessing Java JEE Server management
interfaces which are based on JMX (Java Management Extensions). It is an agent
based approach, where a small Java Webapplication deployed on the application
server provides an HTTP/JSON based access to JMX MBeans registered within the
application server.

This package contains a minimal installation of Jmx4Perl with the jmx4perl
script and the required JMX::* modules.

%prep
%setup -q -n %{cpan_name}-%{jmx4perl_version}
# make sure the examples do not add extra dependencies
chmod 644 examples/*

%build
(echo y; echo n; echo n; echo n; echo n) | %{__perl} Build.PL installdirs=vendor
./Build build flags=%{?_smp_mflags}

%check
./Build test

%install
./Build install destdir=%{buildroot} create_packlist=0
# strip down the installation by removing J4psh, Jolokia and Nagios related files
rm -fr %{buildroot}%{perl_vendorlib}/JMX/Jmx4Perl/J4psh*
rm -fr %{buildroot}%{perl_vendorlib}/JMX/Jmx4Perl/Agent/Jolokia*
rm -fr %{buildroot}%{perl_vendorlib}/JMX/Jmx4Perl/Nagios*
rm -f %{buildroot}%{_mandir}/man3/JMX::Jmx4Perl::J4psh*
rm -f %{buildroot}%{_mandir}/man3/JMX::Jmx4Perl::Agent::Jolokia*
rm -f %{buildroot}%{_mandir}/man3/JMX::Jmx4Perl::Nagios*

%files
%defattr(-,root,root,-)
%doc CHANGES config examples LICENSE README
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/man?/*
